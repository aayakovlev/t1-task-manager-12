package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectService extends BaseService<Project> {

    Project create(final String name);

    Project create(final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

}
