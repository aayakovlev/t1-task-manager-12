package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Task;

public interface TaskRepository extends BaseRepository<Task> {

    Task create(final String name);

    Task create(final String name, final String description);

}
